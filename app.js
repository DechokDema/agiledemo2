const express = require("express")
const path = require('path')
const app = express()
const userRouter = require('./routes/userRoutes')
const viewRouter = require('./routes/viewRoutes')
// Starting the server on port 4001
// const port = 4001
// app.listen(port, () => {
//     console.log(`App running on port ${port}..`)
// })
app.use(express.json())
app.use('/api/v1/users',userRouter)
app.use('/',viewRouter)

app.use(express.static(path.join(__dirname, 'views')))
module.exports = app