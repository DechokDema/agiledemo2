const mongoose = require('mongoose')
const dontenv = require('dotenv')
dontenv.config({path: './config.env'})
const app = require('./app')

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD,
)

const local_DB = process.env.DATABASE_LOCAL
// mongoose.connect(local_DB).them((con)=>{
//     console.log('DB connection succesful')
// }).catch(error => console.log(error))

// console.log(process.env.DATABASE_PASSWORD)
mongoose.connect(local_DB).then((con)=>{
    console.log(con.connections)
    console.log('DB connection succesful')
}).catch(error => console.log(error));

// Starting the server on port 4001.
const port = 4032
app.listen(port, () => {
    console.log(`App running on port ${port}..`)
})